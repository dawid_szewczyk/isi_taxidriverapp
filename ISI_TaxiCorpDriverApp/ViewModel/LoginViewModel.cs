﻿using ISI_TaxiCorpDriverApp.Model;
using ISI_TaxiCorpDriverApp.Model.HttpResponseBody;
using ISI_TaxiCorpDriverApp.Utils;
using ISI_TaxiCorpDriverApp.View;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ISI_TaxiCorpDriverApp.ViewModel
{
    class LoginViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private const string AuthorizationEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
        private const string TokenEndpoint = "https://www.googleapis.com/oauth2/v4/token";
        private const string UserInfoEndpoint = "https://www.googleapis.com/oauth2/v3/userinfo";

        private const string CodeChallengeMethod = "S256";

        private bool waiting;
        public bool Waiting { 
            get { return waiting; }
            set {
                waiting = value;
                OnPropertyChanged(nameof(Waiting));
            }
        }
        public ICommand SignInCommand { get; set; }

        public LoginViewModel() {
            SignInCommand = new RelayCommand(window => SignIn((Window)window));
        }

        public void SignIn(Window window) {
            Waiting = true;
            Login(window);
        }

        public async void Login(Window window) {
            string codeVerifier = RandomDataBase64Url(32);
            string codeChallenge = Base64UrlEncodeNoPadding(Sha256(codeVerifier));

            string redirectUri = string.Format("http://{0}:{1}/", IPAddress.Loopback, GetRandomUnusedPort());
            string state = RandomDataBase64Url(32);
            string requestUri = string.Format("{0}?response_type=code&scope=openid%20profile&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}",
                AuthorizationEndpoint,
                Uri.EscapeDataString(redirectUri),
                Properties.Settings.Default.GoogleOAuthClientId,
                state,
                codeChallenge,
                CodeChallengeMethod);

            HttpListener listener = new HttpListener();
            listener.Prefixes.Add(redirectUri);
            listener.Start();

            Process.Start(requestUri);

            HttpListenerContext context = await listener.GetContextAsync();

            string html = string.Format("<html><head><meta http-equiv='refresh' content='10;url=https://google.com'></head><body>Please return to the app.</body></html>");
            byte[] buffer = Encoding.UTF8.GetBytes(html);
            context.Response.ContentLength64 = buffer.Length;
            Stream stream = context.Response.OutputStream;
            Task responseTask = stream.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                stream.Close();
                listener.Stop();
            });

            Waiting = false;

            if (context.Request.QueryString.Get("error") != null) {
                Logger.AddLine(string.Format("OAuth authorization error: {0}.", context.Request.QueryString.Get("error")));
                Waiting = false;
                return;
            }

            if (context.Request.QueryString.Get("code") == null || context.Request.QueryString.Get("state") == null) {
                Logger.AddLine("Malformed authorization response. " + context.Request.QueryString);
                Waiting = false;
                return;
            }

            var code = context.Request.QueryString.Get("code");
            var incoming_state = context.Request.QueryString.Get("state");

            if (incoming_state != state) {
                Logger.AddLine(string.Format("Received request with invalid state ({0})", incoming_state));
                Waiting = false;
                return;
            }
            UserInfo userInfo = await PerformCodeExchange(code, codeVerifier, redirectUri);

            Logger.AddLine(string.Format("User {0} {1} with id {2} signed in!", userInfo.FirstName, userInfo.LastName, userInfo.Id));

            string serverToken = await GetServerToken();
            await UpdateUserInfo(userInfo, serverToken);

            OpenMainWindow(userInfo, serverToken);
            window.Close();
        }

        async Task<UserInfo> PerformCodeExchange(string code, string code_verifier, string redirectURI) {
            UserInfo userInfo = null;

            string tokenRequestURI = "https://www.googleapis.com/oauth2/v4/token";
            string tokenRequestBody = string.Format("code={0}&redirect_uri={1}&client_id={2}&code_verifier={3}&client_secret={4}&scope=&grant_type=authorization_code",
                code,
                System.Uri.EscapeDataString(redirectURI),
                Properties.Settings.Default.GoogleOAuthClientId,
                code_verifier,
                Properties.Settings.Default.GoogleOAuthClientSecretKey
                );

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(tokenRequestURI);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try {
                // gets the response
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream())) {
                    string responseText = await reader.ReadToEndAsync();

                    Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);

                    string access_token = tokenEndpointDecoded["access_token"];

                    userInfo = await GetUserInfo(access_token);
                }
            } catch (WebException ex) {
                if (ex.Status == WebExceptionStatus.ProtocolError) {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null) {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream())) {
                            string responseText = await reader.ReadToEndAsync();
                            Logger.AddLine("HTTP: " + response.StatusCode + " - " + responseText);
                        }
                    }
                }
            }

            return userInfo;
        }

        async Task<UserInfo> GetUserInfo(string access_token) {
            string userinfoRequestURI = "https://www.googleapis.com/oauth2/v3/userinfo";
            UserInfo userInfo = null;

            HttpWebRequest userinfoRequest = (HttpWebRequest)WebRequest.Create(userinfoRequestURI);
            userinfoRequest.Method = "GET";
            userinfoRequest.Headers.Add(string.Format("Authorization: Bearer {0}", access_token));
            userinfoRequest.ContentType = "application/x-www-form-urlencoded";
            userinfoRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            WebResponse userinfoResponse = await userinfoRequest.GetResponseAsync();
            using (StreamReader userinfoResponseReader = new StreamReader(userinfoResponse.GetResponseStream())) {
                string userinfoResponseText = await userinfoResponseReader.ReadToEndAsync();
                Dictionary<string, string> dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(userinfoResponseText);
                userInfo = new UserInfo(dict);

                Logger.AddLine(string.Join("; ", dict.Select(x => x.Key + " = " + x.Value)));
            }

            return userInfo;
        }

        private async Task<string> GetServerToken() {
            string token = "";

            try {
                LoginController loginController = new LoginController();
                token = await loginController.GetToken(Properties.Settings.Default.ServerUsername, Properties.Settings.Default.ServerPassword); 
            } catch (Exception) {
                Logger.AddLine("Wrong server token response code", LogType.Error);
            }

            return token;
        }

        private async Task UpdateUserInfo(UserInfo userInfo, string token) {
            DriverController driverController = new DriverController();

            if (userInfo.Exists()) {
                string driverId = userInfo.GetDriverIdFromFile();

                try {
                    DriverInfo driverInfo = await driverController.GetDriver(driverId, token);
                    if (driverInfo != null) {
                        userInfo.DriverId = driverId;
                    }
                } catch {
                    try {
                        userInfo.DriverId = await driverController.AddDriver(userInfo, token);
                    } catch {
                        Logger.AddLine("Getting or adding a new driver is not possible!", LogType.Error);
                    }
                }
            } else {
                try {
                    userInfo.DriverId = await driverController.AddDriver(userInfo, token);
                } catch {
                    Logger.AddLine("Adding a new driver is not possible!", LogType.Error);
                }
            }
            userInfo.Save();
        }

        private void OpenMainWindow(UserInfo userInfo, string token) {
            RidesView view = new RidesView(userInfo, token);
            view.Show();
        }

        public static int GetRandomUnusedPort() {
            TcpListener listener = new TcpListener(IPAddress.Loopback, 0);
            
            listener.Start();
            int port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();

            return port;
        }

        private static string RandomDataBase64Url(int length) {
            byte[] bytes = new byte[length];

            using (var rng = new RNGCryptoServiceProvider()) {
                rng.GetBytes(bytes);         
            }

            return Base64UrlEncodeNoPadding(bytes);
        }

        private static string Base64UrlEncodeNoPadding(byte[] buffer) {
            string b64 = Convert.ToBase64String(buffer);

            b64 = b64.Replace('+', '-');
            b64 = b64.Replace('/', '_');

            b64 = b64.Replace("=", "");
            return b64;
        }

        private static byte[] Sha256(string text) {
            using (SHA256Managed sha256 = new SHA256Managed()) {
                return sha256.ComputeHash(Encoding.ASCII.GetBytes(text));
            }
        }

        protected void OnPropertyChanged(string propertyName) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
