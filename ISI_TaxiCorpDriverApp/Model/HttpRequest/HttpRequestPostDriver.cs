﻿using ISI_TaxiCorpDriverApp.Model.HttpResponseBody;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ISI_TaxiCorpDriverApp.Model.HttpRequest
{
    class HttpRequestPostDriver : IHttpRequest
    {
        private const string PostDriverEndpoint = "/driver/add";

        public DriverInfo DriverInfo { get; set; }
        public string Token { get; set; }

        public HttpRequestPostDriver() { }

        public HttpRequestPostDriver(DriverInfo driverInfo, string token) {
            DriverInfo = driverInfo;
            Token = token;
        }

        public HttpRequestPostDriver(string firstName, string lastName, string token) {
            DriverInfo = new DriverInfo {
                FirstName = firstName,
                LastName = lastName
            };
            Token = token;
        }

        public HttpContent GetRequestContent() {
            string json = JsonConvert.SerializeObject(DriverInfo);

            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        public string GetRequestUri() {
            return string.Concat(Properties.Settings.Default.ISIApiServerUrl, PostDriverEndpoint);
        }

        public AuthenticationHeaderValue GetAuthenticationHeaderValue() {
            return new AuthenticationHeaderValue(Properties.Settings.Default.BearerTokenKey, Token);
        }
    }
}

