﻿using ISI_TaxiCorpDriverApp.Model.HttpRequestBody;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ISI_TaxiCorpDriverApp.Model.HttpRequest
{
    class HttpRequestRideTake : IHttpRequest
    {
        private const string RidesEndpoint = "/ride/linkToDriver";

       public RideCredentials rideCredentials { get; set; }
        public string Token { get; set; }

        public HttpRequestRideTake() { }

        public HttpRequestRideTake(string token, long driverId, long rideId)
        {
            rideCredentials = new RideCredentials();
            rideCredentials.DriverId = driverId;
            rideCredentials.RideId = rideId;
            this.Token = token;
        }


        public string GetRequestUri()
        {
            string parameters = GetRequestContent().ReadAsStringAsync().Result;

            return string.Format("{0}?{1}", Properties.Settings.Default.ISIApiServerUrl + RidesEndpoint, parameters);
        }

        public HttpContent GetRequestContent()
        {
            Dictionary<string, string> requestParameters = new Dictionary<string, string> {
                { "rideId", rideCredentials.RideId.ToString() },
                { "driverId", rideCredentials.DriverId.ToString() }
            };

            return new FormUrlEncodedContent(requestParameters);
        }

        //public HttpContent GetRequestContent() {
        //    string json = JsonConvert.SerializeObject(rideCredentials);

        //    return new StringContent(json, Encoding.UTF8, "application/json");
        //}

        //public string GetRequestUri()
        //{
        //    return string.Concat(Properties.Settings.Default.ISIApiServerUrl, RidesEndpoint);
        //}

        public AuthenticationHeaderValue GetAuthenticationHeaderValue()
        {
            return new AuthenticationHeaderValue(Properties.Settings.Default.BearerTokenKey, Token);
        }
    }
}
