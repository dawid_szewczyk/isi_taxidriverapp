﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ISI_TaxiCorpDriverApp.Model.HttpRequest
{
    class HttpRequestGetDriver : IHttpRequest
    {
        private const string GetDriverEndpoint = "/driver/get";
        private const string IdKey = "id";

        public string Id { get; set; }
        public string Token { get; set; }

        public HttpRequestGetDriver() { }

        public HttpRequestGetDriver(string id, string token) {
            Id = id;
            Token = token;
        }

        public HttpContent GetRequestContent() {
            return null;
        }

        public string GetRequestUri() {
            Dictionary<string, string> requestParameters = new Dictionary<string, string> {
                { IdKey, Id }
            };

            FormUrlEncodedContent encodedParameters = new FormUrlEncodedContent(requestParameters);

            string parameters = encodedParameters.ReadAsStringAsync().Result;

            return string.Format("{0}{1}?{2}", Properties.Settings.Default.ISIApiServerUrl, GetDriverEndpoint, parameters);
        }

        public AuthenticationHeaderValue GetAuthenticationHeaderValue() {
            return new AuthenticationHeaderValue(Properties.Settings.Default.BearerTokenKey, Token);
        }
    }
}
