﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISI_TaxiCorpDriverApp.Model.HttpRequestBody
{
    public class RideCredentials
    {
        [JsonProperty("rideId")]
        public long RideId { get; set; }
        [JsonProperty("driverId")]
        public long DriverId { get; set; }
    }
}
