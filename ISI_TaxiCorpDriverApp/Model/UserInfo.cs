﻿using ISI_TaxiCorpDriverApp.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISI_TaxiCorpDriverApp.Model
{
    public class UserInfo
    {
        private const string UsersFilename = "users.txt";

        public string Id { get; set; }
        public string DriverId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public UserInfo() { }

        public UserInfo(string id, string firstName, string lastName, string email) {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public UserInfo(Dictionary<string, string> userInfoDict) {
            userInfoDict.TryGetValue("sub", out string id);
            userInfoDict.TryGetValue("given_name", out string firstName);
            userInfoDict.TryGetValue("family_name", out string lastName);
            userInfoDict.TryGetValue("email", out string email);

            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public bool Exists() {
            bool result = false;
            if (File.Exists(UsersFilename)) {
                foreach (string line in File.ReadAllLines(UsersFilename)) {
                    string[] words = line.Split(' ');
                    if (words.Length > 0) {
                        result = words[0] == Id;
                    }
                }
            }
            return result;
        }

        public string GetDriverIdFromFile() {
            string result = "";
            if (File.Exists(UsersFilename)) {
                foreach (string line in File.ReadAllLines(UsersFilename)) {
                    string[] words = line.Split(' ');
                    if (words.Length > 1 && words[0] == Id) {
                        result = words[1];
                    }
                }
            }
            return result;
        }

        public void Save() {
            if (Id == null || Id == "") {
                Logger.AddLine("User id is empty!", LogType.Warning);
                return;
            }

            if (DriverId == null || DriverId == "") {
                Logger.AddLine("Driver id is empty!", LogType.Warning);
                return;
            }

            using (StreamWriter sw = File.AppendText(UsersFilename)) {
                string line = string.Format("{0} {1} {2} {3} {4}", Id, DriverId, FirstName, LastName, Email);
                sw.WriteLine(line);
            }
        }
    }
}
