﻿using ISI_TaxiCorpDriverApp.Model.HttpRequest;
using ISI_TaxiCorpDriverApp.Model.HttpResponseBody;
using ISI_TaxiCorpDriverApp.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ISI_TaxiCorpDriverApp.Model
{
    class DriverController
    {
        public async Task<DriverInfo> GetDriver(string id, string token) {
            DriverInfo driverInfo = null;

            HttpResponseMessage driverResponse = await GetDriverResponse(id, token);

            switch (driverResponse.StatusCode) {
                case HttpStatusCode.OK:
                    string driverResponseBody = await driverResponse.Content.ReadAsStringAsync();       

                    try {
                        driverInfo = JsonConvert.DeserializeObject<DriverInfo>(driverResponseBody);
                    } catch (Exception) {
                        Logger.AddLine("Driver not found!");
                    }  
                    break;

                default:
                    throw new Exception();
            }

            return driverInfo;
        }

        private async Task<HttpResponseMessage> GetDriverResponse(string id, string token) {
            HttpRequestGetDriver getDriverRequest = new HttpRequestGetDriver(id, token);

            Logger.AddLine(getDriverRequest.GetRequestUri());

            HttpResponseMessage getDriverResponse = await HttpClientManager.GetAsync(getDriverRequest);

            HttpClientManager.LogRequestResponseAsync(getDriverResponse);

            return getDriverResponse;
        }

        public async Task<string> AddDriver(UserInfo userInfo, string token) {
            string driverId = "";

            DriverInfo driverInfo = new DriverInfo {
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName
            };

            HttpResponseMessage driverResponse = await PostDriverResponse(driverInfo, token);

            switch (driverResponse.StatusCode) {
                case HttpStatusCode.OK:
                    string driverResponseBody = await driverResponse.Content.ReadAsStringAsync();

                    try {
                        driverId = driverResponseBody.Split(' ').Last();
                    } catch (Exception) {
                        Logger.AddLine("Wrong format of driver posting response message");
                    }
                    break;

                default:
                    throw new Exception();
            }

            return driverId;
        }

        private async Task<HttpResponseMessage> PostDriverResponse(DriverInfo driverInfo, string token) {
            HttpRequestPostDriver postDriverRequest = new HttpRequestPostDriver(driverInfo, token);

            Logger.AddLine(postDriverRequest.GetRequestUri());

            HttpResponseMessage postDriverResponse = await HttpClientManager.PostAsync(postDriverRequest);

            HttpClientManager.LogRequestResponseAsync(postDriverResponse);

            return postDriverResponse;
        }
    }
}
