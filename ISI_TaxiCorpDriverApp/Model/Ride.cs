﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ISI_TaxiCorpDriverApp.Model
{
    [DataContract]
    class Ride
    {
        [JsonProperty("id")]
        public int id{ get; set; }
        [JsonProperty("originLat")]
        public double startLat{ get; set; }
        [JsonProperty("originLon")]
        public double startLon { get; set; }
        [JsonProperty("destinationLat")]
        public double destLat { get; set; }

        [JsonProperty("destinationLon")]
        public double destLon { get; set; }
        [JsonProperty("client")]
        public Client client{ get; set; }
        public double distance { get; set; }
        [JsonProperty("price")]
        public double price { get; set; }
        public string customerName {
            get {
                if (client != null)
                {
                    return client.firstName + " " + client.lastName;
                }
                else
                    return "";
            }
        }
        public Ride ()
        {

        }
        //public Ride(WorldPosition _initPos, WorldPosition _destPos, string _custName, double _distance = 0)
        //{
        //    startPosition       = _initPos;
        //    destinationPosition = _destPos;
        //    customerName        = _custName;
        //    distance            = _distance;
        //}
    }
}
